<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin')->except('index');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend/dashboard');
    }
	
	public function widget_preview()
    {
		$style = get_option('widget_style','modern');
		return view("widgets/$style/widget-preview");	
    }
	
	public function widget_code()
    {
       return '<pre><textarea class="form-control" style="resize: none; text-align:center;" readOnly="true"><script src="'.url('chat_widget.js').'"></script></textarea></pre>';
    }
	
}
